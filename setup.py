from distutils.core import setup
setup(name='managementagent',
      version='0.1a1',
      description='DeployIT Managementagent modules and scripts',
      author='Rafael Ostertag',
      author_email='support@math.uzh.ch',
      packages=['managementagent'],
      scripts=['bin/deployitagent.py'])
