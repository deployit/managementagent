""" Base class for Rabbit modules"""
import deployit.message
import deployit.modules
import deployit.rabbit
import deployit.deployitlogging
import json
import logging
import os
import pika
import threading
import time

logger = logging.getLogger(deployit.deployitlogging.logger_name)

class RabbitModule(deployit.Module):
    """Base class for Management Agents

    Remember: call deployit.config.read() before starting this class as thread.
    """

    def __init__(self, name, resourcename, version):
        super(RabbitModule, self).__init__(name, resourcename, version)

    def _setup_signal_handler(self):
        signal.signal(signal.SIGTERM, self._signal_handler)
        signal.signal(signal.SIGQUIT, self._signal_handler)
        signal.signal(signal.SIGINT, self._signal_handler)
        signal.signal(signal.SIGHUP, self._signal_handler)

    def _signal_handler(self, signum, frame):
        logger.debug('Shutting down %s', self.name)
        try:
            self.cancel_consume()
        except:
            pass
        try:
            self.close()
        except:
            pass

    def __call__(self, **kwargs):
        """Used when run as thread"""
        if ('setupsignalhandler' in kwargs and kwargs['setupsignalhandler']==True):
            self._setup_signal_handler()

        keep_running = True
        retry_counter = 0
        while keep_running:
            try:
                self.rabbit=deployit.Rabbit()

                with self.rabbit as rabbit:
                    logger.debug("Start consuming %s", self.name)
                    rabbit.consume_from_fanout(self.resourcename,
                                               self._prepare_message)

                    rabbit.start_consuming()
                    keep_running = False
            except pika.exceptions.AMQPConnectionError:
                retry_counter = retry_counter + 1
                logger.warning("Connection error. Retrying (%d)...", retry_counter)
                time.sleep(kwargs['reconnect_interval'])
            except Exception as ex:
                logger.error("Error initializing rabbit: %s", repr(ex))
                return

        logger.debug("Stopped consuming %s", self.name)

    def _prepare_message(self, channel, method, header, body):
        if header.content_type != deployit.rabbit.MSG_CONTENT_TYPE:
            channel.basic_nack(delivery_tag=method.delivery_tag,
                               requeue=False)
            logger.error("Received message with content type '%s' instead of '%s'",
                         header.content_type,
                         deployit.rabbit.MSG_CONTENT_TYPE)
        else:
            channel.basic_ack(delivery_tag=method.delivery_tag)

        try:
            message = deployit.Message(body)
            logger.info("Going to process message from %s", (message.header['created-by']))
            self.process_message(message)
        except Exception as ex:
            self.send_reply(deployit.Message(body).header['uuid'], -1, "Caught exception: %s" % (repr(ex),))

    def send_reply(self, reply_to_uuid, statuscode, body):
        message = self.message_template()
        message.header['in-reply-to'] = reply_to_uuid
        message.header['status-code'] = int(statuscode)
        message.body = body
        self.rabbit.publish_to_direct(deployit.rabbit.MANAGEMENTHOST_EXCHANGE,
                                      message)

    def cancel_consume(self):
        self.rabbit.stop_consuming()

    def close(self):
        self.rabbit.close()
