import shutil
import datetime
import logging
import os
import deployit.deployitlogging

logger = logging.getLogger(deployit.deployitlogging.logger_name)

class BackupFile(object):
    """Create a backup files

    It goes like this:

      with BackupFile(<filename>):
        <do stuff here>

    If no error occurs, the backup file will be deleted. If an error
    occurs, the backup file will be renamed to <filename>.
    """

    def __init__(self, sourcefile):
        self.sourcefile = sourcefile
        self.firstfile = None
        self.backupfiles = list()

    def __enter__(self):
        self.create()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if exc_type is None and exc_value is None and traceback is None:
            self.remove_backup()
        else:
            self.restore()

    def create(self):
        timestamp = datetime.datetime.strftime(datetime.datetime.now(),
                                               BackupFile.timestamp_format)
        backup_filename = self.sourcefile + "." + timestamp

        shutil.copy(self.sourcefile, backup_filename)

        if not self.firstfile:
            self.firstfile = backup_filename

        self.backupfiles.append(backup_filename)

    def restore(self):
        assert self.firstfile is not None, "Cannot restore since no backup file has been created"

        shutil.move(self.firstfile, self.sourcefile)

        # Purge firstfile from list of backupfiles, so that
        # remove_backup() won't try to delete a non-existent file.
        del self.backupfiles[self.backupfiles.index(self.firstfile)]
        self.remove_backup()

    def remove_backup(self):
        [os.remove(backup_file) for backup_file in self.backupfiles]

    timestamp_format = '%Y-%m-%dT%H:%M:%s'
