"""Module Configuration"""

import os.path
import deployit.config
import deployit.deployitlogging
import ConfigParser
import logging

logger = logging.getLogger(deployit.deployitlogging.logger_name)

class ModuleConfig(object):
    def __init__(self, modulename):
        assert deployit.config.modules is not None, "deployit.config.read() must be called first"
        self.modulename = modulename
        self.module_config_file = os.path.join(deployit.config.modules['path'],
                                               ModuleConfig.config_filename)

    def get_config(self):
        try:
            cparser = ConfigParser.ConfigParser()
            cparser.read(self.module_config_file)

            return cparser.items(self.modulename)
        except Exception as e:
            logging.warning(repr(e))
            return None

    config_filename = "modules.ini"
