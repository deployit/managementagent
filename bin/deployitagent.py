#!/usr/bin/env python

import argparse
import deployit.config
import deployit.modules
import deployit.utils
import logging
import logging.handlers
import managementagent.rabbitmodules
import os
import signal
import sys
import threading
import time

children = list()
logger = None

def _signal_handler(signum, frame):
    shutdown()

def setup_argumentparser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--cfgfile',
                        help='configuration file',
                        default='/etc/deployit/agent.cfg',
                        required=False)
    parser.add_argument('--foreground',
                        help="don't fork to background",
                        action="store_const",
                        default=False,
                        const=True)

    return parser

def shutdown():
    global logger, children

    logger.info('Stopping consumers')
    logger.info('Hangup children')
    [ os.kill(cpid, signal.SIGHUP) for cpid in children ]

    logger.info('Wait for children')
    [ os.waitpid(cpid, 0) for cpid in children]

    logger.info('Shut down')

def setup_signal_handler():
    global logger

    logger.debug('Setting up signal handlers')
    signal.signal(signal.SIGTERM, _signal_handler)
    signal.signal(signal.SIGQUIT, _signal_handler)
    signal.signal(signal.SIGINT, _signal_handler)

def setup_syslog_logging(logger_name):
    logger= logging.getLogger(logger_name)

    syslog_handler = logging.handlers.SysLogHandler(address='/dev/log',
                                                        facility='daemon')
    syslog_handler.setLevel(deployit.utils.str_to_loglevel(deployit.config.agent['loglevel']))
    formatter = logging.Formatter('%(name)s: %(message)s')
    syslog_handler.setFormatter(formatter)

    logger.addHandler(syslog_handler)

def main():
    global logger, children

    arguments = setup_argumentparser().parse_args()
    deployit.config.read(arguments.cfgfile)

    logger = logging.getLogger(deployit.deployitlogging.logger_name)
    logger.setLevel(deployit.utils.str_to_loglevel(deployit.config.agent['loglevel']))

    if not arguments.foreground:
        pid = os.fork()
        if pid != 0:
            # The parent
            sys.exit(0)

        # The child
        os.setsid()

        with open(deployit.config.agent['pidfile'], 'w') as pidfile:
            pidfile.write(str(os.getpid()))

        setup_syslog_logging(deployit.deployitlogging.logger_name)

        logger.debug('Going to redirect stdin, stdout, stderr')
        dev_null_fd = os.open('/dev/null', os.O_RDWR)
        os.dup2(dev_null_fd, sys.stdin.fileno())
        os.dup2(dev_null_fd, sys.stdout.fileno())
        os.dup2(dev_null_fd, sys.stderr.fileno())

        os.close(dev_null_fd)

        logger.info("Daemonized deployit agent")
    else:
        ch = logging.StreamHandler()
        ch.setLevel(deployit.utils.str_to_loglevel(deployit.config.agent['loglevel']))
        logger.addHandler(ch)
        logger.debug('Stay in foreground')
        logging.basicConfig(level=deployit.utils.str_to_loglevel(deployit.config.agent['loglevel']))

    deployit.modules.init_modules(deployit.config.modules['path'])

    setup_signal_handler()

    logger.debug('start consuming')

    try:
        for module in deployit.modules.modulelist:
            pid = os.fork()
            if pid == 0:
                deployit.modules.modulelist[module]['instance'](reconnect_interval=deployit.config.agent['reconnect_interval']) # run the module
                exit(0)

            children.append(pid)

        logger.debug('going into signal.pause()')
        signal.pause()

    finally:
        if not arguments.foreground:
            logger.debug('Unlink pid file %s', deployit.config.agent['pidfile'])
            os.remove(deployit.config.agent['pidfile'])

if __name__ == "__main__":
    main()

