Requirements
============

 * pika (https://github.com/pika/pika)
 * deployit common (https://git.math.uzh.ch/deployit/common)


Ansible Playbook
================

The ansible playbook deployit-managementagent.yml in the SVN repo
file:///scratch/svn/it/ansible/playbooks can be used to install the
management agent.
