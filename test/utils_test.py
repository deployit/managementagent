import managementagent.utils
import os.path
import unittest
import time

class TestBackupFile(unittest.TestCase):

    def setUp(self):
        self.sourcename = '/tmp/TestBackupFile.txt'
        with open(self.sourcename, 'w') as fobj:
            fobj.write('Line 1, no backup\n')

    def write_line(self):
        with open(self.sourcename, 'a') as fobj:
            fobj.write('Line 2, backup file created\n')

    def test_backup_success_context(self):
        backupfiles = None
        with managementagent.utils.BackupFile(self.sourcename) as backup_file:
            self.write_line()

            backupfiles = backup_file.backupfiles
            self.assertEqual(len(backupfiles), 1)
            self.assertTrue(os.path.exists(backupfiles[0]))

        self.assertFalse(os.path.exists(backupfiles[0]))

        lines = None
        with open(self.sourcename, 'r') as fobj:
            lines = fobj.readlines()

        self.assertIn('Line 1, no backup\n', lines)
        self.assertIn('Line 2, backup file created\n', lines)

    def test_backup_failure_context(self):
        backupfiles = None
        try:
            with managementagent.utils.BackupFile(self.sourcename) as backup_file:
                self.write_line()

                # make it fail
                assert 0 == 1
        except:
            pass

        lines = None
        with open(self.sourcename, 'r') as fobj:
            lines = fobj.readlines()

        self.assertIn('Line 1, no backup\n', lines)
        self.assertNotIn('Line 2, backup file created\n', lines)

    def test_backup_no_context(self):
        # Create backup of default
        backupfile = managementagent.utils.BackupFile(self.sourcename)
        backupfile.create()

        first_backup = backupfile.firstfile
        self.assertTrue(os.path.exists(first_backup))

        # update original
        self.write_line()

        # Backup timestamp may be of low resolution
        time.sleep(1)

        backupfile.create()

        # BackupFile.firstfile must not be changed
        self.assertEqual(first_backup, backupfile.firstfile)

        # BackupFile.backupfiles now must have two entries
        self.assertEqual(len(backupfile.backupfiles), 2)

        # The first backup file must have only one line
        lines = None
        with open(backupfile.backupfiles[0], 'r') as fobj:
            lines = fobj.readlines()

        self.assertIn('Line 1, no backup\n', lines)
        self.assertNotIn('Line 2, backup file created\n', lines)

        # The second backup file must have two lines
        lines = None
        with open(backupfile.backupfiles[1], 'r') as fobj:
            lines = fobj.readlines()

        self.assertIn('Line 1, no backup\n', lines)
        self.assertIn('Line 2, backup file created\n', lines)

        # Assume success, no backup files must be linger around.
        backupfiles = backupfile.backupfiles
        backupfile.remove_backup()

        [self.assertFalse(os.path.exists(f)) for f in backupfiles]


if __name__ == "__main__":
    unittest.main()
