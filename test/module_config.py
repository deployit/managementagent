import managementagent.moduleconfig
import deployit.config
import unittest


class TestModuleConfig(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        deployit.config.read('agent.cfg')

    def test_module_config(self):
        mc = managementagent.ModuleConfig('testmodule')
        config = dict(mc.get_config())

        self.assertIn('key1', config)
        self.assertIn('key2', config)
        self.assertEqual(config['key1'], "bla")
        self.assertEqual(config['key2'], "blabla")

if __name__ == "__main__":
    unittest.main()
