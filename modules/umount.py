import managementagent.rabbitmodules
import managementagent.utils
import os
import os.path
import logging
import re
import shutil
import subprocess
import pwd
import stat
import deployit.deployitlogging

__author__ = 'Benjamin Baer <benjamin.baer@math.uzh.ch>'

logger = logging.getLogger(deployit.deployitlogging.logger_name )


class Umount(managementagent.RabbitModule):
    """Handling a umount request"""

    def __init__(self):
        super(Umount, self).__init__('Umount', 'umount', 1)

    def process_message(self, message):
        instructions = message.body
        reply_to = message.header['uuid']

        try:
            logger.debug('process message: %s', message)

            directory = instructions['directory']
            logger.debug('trying to restart service %s', directory)

            process = subprocess.Popen(['/bin/umount', '-f', directory])
            process.wait()

            if process.returncode != 0:
                logger.warning("Failed to umount directory %s", directory)
                self.send_reply(reply_to, process.returncode, "Error executing umount -f command (%i)" % (process.returncode,))
            else:
                logger.info("Unmounting of the directory %s successful", directory)
                self.send_reply(reply_to, 0, "umount -f %s successful" % (directory,))

        except Exception as ex:
            self.send_reply(reply_to, -1, repr(ex))

def initialize():
    return Umount().identification()
