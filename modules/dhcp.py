import managementagent.rabbitmodules
import managementagent.utils
import os
import os.path
import logging
import re
import shutil
import subprocess
import deployit.deployitlogging

logger = logging.getLogger(deployit.deployitlogging.logger_name)

class DhcpSyntaxError(Exception):
    def __init__(self):
        super(DhcpSyntaxError, self).__init__()

    def __str__(self):
        return "Syntax error in dhcpd configuration"

class DhcpRestartError(Exception):
    def __init__(self):
        super(DhcpRestartError, self).__init__()

    def __str__(self):
        return "error restarting isc-dhcp-server"


class Dhcp(managementagent.RabbitModule):
    """Handling a ping request"""

    def __init__(self):
        super(Dhcp, self).__init__('DHCP', 'dhcp', 1)

    def _dhcp_host_tag_from_mac(self, macaddr):
        return ("".join(macaddr.split(':'))).lower()

    def _compose_dhcp_config(self, macaddresses):
        config_array = list()
        for macaddr in macaddresses:
            if Dhcp.proper_mac_re.match(macaddr) is None:
                logging.warning('Malformed MAC address: %s. Ignoring.', macaddr)
                continue

            config_array.append("host %s { hardware ethernet %s ; }" % (self._dhcp_host_tag_from_mac(macaddr), macaddr))

        return '\n'.join(config_array)

    def _process_replace_request(self, message):
        destinationfile = message.body['dhcp_include_file']
        tempfile = destinationfile + '.in-progress'

        with managementagent.utils.BackupFile(destinationfile):
            with open(tempfile, 'w') as fobj:
                fobj.write(self._compose_dhcp_config(message.body['add']))

            # Move the temporary file in place
            shutl.move(tempfile, destinationfile)

            # Test the syntax of the configuration
            retcode = subprocess.call("dhcpd -t")
            if retcode != 0:
                # There was an error. Bail out and let the context
                # manager handle restoration of the original file
                raise DhcpSyntaxError()

            # restart dhcpd
            retcode = subprocess.call("service isc-dhcp-server restart")
            if retcode != 0:
                raise DhcpRestartError()

    def process_message(self, message):
        instructions = message.body
        reply_to = message.header['uuid']

        dhcp_include_file = instructions['dhcp_include_file']
        if not os.path.exist(dhcp_include_file):
            self.send_reply(reply_to, 2,
                            "dhcp_include_file '%s' not found. Aborting." % (dhcp_include_file,))
            return

        logger.debug('process message: %s', message)

        if instructions['replace'] == True and \
                'add' not in instructions:
            self.send_reply(reply_to, 3, "Action request was 'replace', but no 'add' item was found")
            return

        if instructions['replace'] == True:
            _process_replace_request(message)

    proper_mac_re = re.compile(r'^[a-fA-F0-9]{1,2}:[a-fA-F0-9]{1,2}:[a-fA-F0-9]{1,2}:[a-fA-F0-9]{1,2}:[a-fA-F0-9]{1,2}:[a-fA-F0-9]{1,2}$')


def initialize():
    return Dhcp().identification()
