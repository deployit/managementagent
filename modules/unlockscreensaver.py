import managementagent.rabbitmodules
import managementagent.utils
import os
import os.path
import logging
import re
import shutil
import subprocess
import pwd
import stat
import deployit.deployitlogging

__author__ = 'Rafael Ostertag <rafael.ostertag@math.uzh.ch>'

logger = logging.getLogger(deployit.deployitlogging.logger_name )

def find_process(username, command):
    """
    Find a process belonging to a user by its base name

    :param username: the username of the user the process belongs to
    :param command: the base name of the command to look up
    :return: a dict holding the pid and environment of the process
    """
    try:
        uid = pwd.getpwnam(username)[2]
    except:
        return None

    # get all process directories for the given user
    processdirectories = [procdir for procdir in os.listdir('/proc')
                          if os.path.isdir(os.path.join('/proc', procdir)) and
                          re.match(r'^[\d]+$', procdir) and
                          os.stat(os.path.join('/proc', procdir))[stat.ST_UID] == uid]

    for processdir in processdirectories:
        # get the full path of the command. Using /proc/<pid>/comm did not pan out, since it is truncated
        try:
            commandpath = os.readlink(os.path.join('/proc', processdir, 'exe'))
        except OSError:
            continue

        # we're only interested in the basename of the command
        commandname = os.path.basename(commandpath)
        if command == commandname:
            # we need the environment in a dict.
            #
            # TODO: make this more comprehensible by using list/dict comprehensions
            with open(os.path.join('/proc', processdir, 'environ'), 'r') as envfile:
                environmentarr = envfile.readline().rstrip('\0\n').split('\0')
                environmentdict = {}
                for environentry in environmentarr:
                    tmp = environentry.split('=', 1)
                    environmentdict[tmp[0]] = tmp[1]

            return {'pid': int(processdir), 'environ': environmentdict}

    return None


class UnlockScreensaver(managementagent.RabbitModule):
    """Handling a ping request"""

    def __init__(self):
        super(UnlockScreensaver, self).__init__('UnlockScreensaver', 'unlockscreensaver', 1)

    def process_message(self, message):
        instructions = message.body
        reply_to = message.header['uuid']

        try:
            logger.debug('process message: %s', message)

            username = instructions['username']
            logger.debug('trying to find screensaver for %s', username)

            screensaverinfo = find_process(username, 'mate-screensaver')
            if screensaverinfo is None:
                self.send_reply(reply_to, -1, 'Unable to find mate-screensaver for %s' %(username,))
                return

            process = subprocess.Popen(['/bin/su', username, '-c',  '/usr/bin/mate-screensaver-command --deactivate'], env=screensaverinfo['environ'])
            process.wait()

            if process.returncode != 0:
                logger.warning("Failed to unlocked screensaver for %s", username)
                self.send_reply(reply_to, process.returncode, "Error executing mate-screensaver-command (%i)" % (process.returncode,))
            else:
                logger.info("Unlocked screensaver for %s", username)
                self.send_reply(reply_to, 0, "Unlocked screen for %s" % (username,))

        except Exception as ex:
            self.send_reply(reply_to, -1, repr(ex))

def initialize():
    return UnlockScreensaver().identification()
