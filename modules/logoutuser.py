import managementagent.rabbitmodules
import managementagent.utils
import os
import os.path
import logging
import re
import shutil
import subprocess
import time
import pwd
import stat
import deployit.deployitlogging

__author__ = 'Benjamin Baer <benjamin.baer@math.uzh.ch>'

logger = logging.getLogger(deployit.deployitlogging.logger_name )


class LogoutUser(managementagent.RabbitModule):
    """Handling a logout request"""

    def __init__(self):
        super(LogoutUser, self).__init__('LogoutUser', 'logoutuser', 1)

    def process_message(self, message):
        instructions = message.body
        reply_to = message.header['uuid']

        try:
            logger.debug('process message: %s', message)

            usernames = instructions['usernames']

            for username in usernames:
                logger.debug('trying to kill user %s', username)

                returnCode = subprocess.call(['/usr/bin/pkill', '-u', username, ' tl-xinit'])
                time.sleep(5)
                returnCode2 = subprocess.call(['/usr/bin/pkill', '-u', username, '-KILL'])

                if returnCode == 0:
                    logger.info("User %s logout succesfull", username)
                    self.send_reply(reply_to, 0, "Ended tl session for %s" % (username,))
                elif returnCode == 1:
                    logger.warning("User %s not logged in", username)
                    self.send_reply(reply_to, returnCode, "pkill command couldn't find a tl-xinit process (%i) for user %s" % (returnCode, username))
                else:
                    logger.warning("Failed to logout user %s", username)
                    self.send_reply(reply_to, returnCode, "Error executing pkill tl-xinit command (%i) for user %s" % (returnCode, username))

                if returnCode2 == 0:
                    logger.info("User %s processes killed", username)
                    self.send_reply(reply_to, 0, "Killed processes for %s" % (username,))
                elif returnCode2 == 1:
                    logger.warning("User %s no processes found", username)
                    self.send_reply(reply_to, returnCode, "pkill command did find no matching processes (%i) for user %s" % (returnCode2, username))
                else:
                    logger.warning("Failed to logout user %s", username)
                    self.send_reply(reply_to, returnCode, "Error executing pkill command (%i) for user %s" % (returnCode2, username))

        except Exception as ex:
            self.send_reply(reply_to, -1, repr(ex))

def initialize():
    return LogoutUser().identification()
