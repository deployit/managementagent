import managementagent.rabbitmodules
import os
import logging
import deployit.deployitlogging

logger = logging.getLogger(deployit.deployitlogging.logger_name)

class Ping(managementagent.RabbitModule):
    """Handling a ping request"""

    def __init__(self):
        super(Ping, self).__init__('Ping', 'ping', 1)

    def process_message(self, message):
        logger.info('process message: %s', message)
        self.send_reply(message.header['uuid'], 0, "host %s alive" % (os.uname()[1]))


def initialize():
    return Ping().identification()
