import managementagent.rabbitmodules
import managementagent.moduleconfig
import os
import subprocess
import logging
import json
import deployit.deployitlogging

logger = logging.getLogger(deployit.deployitlogging.logger_name)

class GenericCommand(managementagent.RabbitModule):
    """Handling Generic Command Request"""

    def __init__(self):
        super(GenericCommand, self).__init__('GenericCommand', 'genericcommand', 1)

    def process_message(self, message):
        reply_to = message.header['uuid']
        logger.debug('process message: %s', message)

        logger.debug("obtaining module configuration for '%s'", self.name)
        configuration = managementagent.ModuleConfig(self.name).get_config()

        if configuration is None:
            errmsg = "No configuration for '%s' found. Do nothing." % (self.name,)
            logger.warning(errmsg)
            self.send_reply(reply_to, -1, errmsg)
            return

        # Convert the [(key,value),...] list to a dict for better digestion
        configuration = dict(configuration)

        if not 'tags' in configuration:
            errmsg = "'tags' not in '%s' configuration." % (self.name,)
            logger.warning(errmsg)
            self.send_reply(reply_to, -1, errmsg)
            return

        if not message.body['tag'] in configuration['tags'].split(':'):
            logger.debug("Received message not intended for us")
            return

        try:
            command = [message.body['command']]
            command.extend(message.body['arguments'])

            # Apparently, if we use a shell, we have to pass one
            # string. If we don't use the shell, a list of arguments
            # is expected.
            if message.body['useshell']:
                command = " ".join(command)

            if not message.body['capture_output']:
                retcode = subprocess.call(command,
                                          shell=message.body['useshell'])
                stdout = stderr = 'Not captured'
            else:
                process = subprocess.Popen(command, shell=message.body['useshell'],
                                           stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                (stdout, stderr) = process.communicate()
                retcode = process.returncode
            self.send_reply(reply_to, retcode, json.dumps({ 'stdout': stdout, 'stderr': stderr }))
        except Exception as ex:
            self.send_reply(reply_to, -1, repr(ex))


def initialize():
    return GenericCommand().identification()
