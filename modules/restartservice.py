import managementagent.rabbitmodules
import managementagent.utils
import os
import os.path
import logging
import re
import shutil
import subprocess
import pwd
import stat
import deployit.deployitlogging

__author__ = 'Benjamin Baer <benjamin.baer@math.uzh.ch>'

logger = logging.getLogger(deployit.deployitlogging.logger_name )


class RestartService(managementagent.RabbitModule):
    """Handling a service restart request"""

    def __init__(self):
        super(RestartService, self).__init__('RestartService', 'restartservice', 1)

    def process_message(self, message):
        instructions = message.body
        reply_to = message.header['uuid']

        try:
            logger.debug('process message: %s', message)

            servicename = instructions['servicename']
            logger.debug('trying to restart service %s', servicename)

            process = subprocess.Popen(['/usr/bin/service', servicename, 'restart'])
            process.wait()

            if process.returncode != 0:
                logger.warning("Failed to restart service %s", servicename)
                self.send_reply(reply_to, process.returncode, "Error executing service command (%i)" % (process.returncode,))
            else:
                logger.info("Service %s restarted successful", servicename)
                self.send_reply(reply_to, 0, "Service %s restarted" % (servicename,))

        except Exception as ex:
            self.send_reply(reply_to, -1, repr(ex))

def initialize():
    return RestartService().identification()
