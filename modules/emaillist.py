import logging
import subprocess

import managementagent.rabbitmodules
import managementagent.utils
import deployit.deployitlogging


__author__ = 'Rafael Ostertag <rafael.ostertag@math.uzh.ch>'

logger = logging.getLogger(deployit.deployitlogging.logger_name)


class EmailList(managementagent.RabbitModule):
    """Use Zimbra zmprov to maintain mailinglists

    TODO: Return error messages produced by zmprov.
    """

    def __init__(self):
        super(EmailList, self).__init__('EmailList', 'emaillist', 1)

    def _process_lists_operations(self, list_operations):
        zmprov = subprocess.Popen(EmailList.ZIMBRA_ZMPROV, shell=False, stdin=subprocess.PIPE, stdout=subprocess.PIPE)

        zmprov_lines = list()
        for operation in list_operations:
            op = ""
            if operation['operation'] == 'create':
                op = 'cdl'
            if operation['operation'] == 'delete':
                op = 'ddl'

            if op == "":
                continue

            zmprov_lines.append(op + " " + operation['name'] + "\n")
            if 'displayname' in operation and op == 'cdl':
                zmprov_lines.append('modifyDistributionList %s displayName "%s"\n' % (operation['name'], operation['displayname']))

        zmprov.communicate(input="".join(zmprov_lines))

    def _create_members_set_command(self, list_name, members):
        return_value = ""

        operation = "zimbraMailForwardingAddress"
        for member in members:
            return_value += "modifyDistributionList %s %s %s\n" % (list_name, operation, member)

            # if there is no '+' in front of zimbraMailForwardingAddress, it is the first iteration and we switch
            # to '+zimbraMailForwardingAddress for subsequent iterations, in order to append entries to the list,
            # instead of setting it the member
            if operation[0] != '+':
                operation = "+" + operation

        return return_value

    def _create_members_add_command(self, list_name, members):
        return self._create_command("addDistributionListMember", list_name, members)

    def _create_members_remove_command(self, list_name, members):
        return self._create_command("removeDistributionListMember", list_name, members)

    def _create_command(self, command, list_name, members):
        return_value = ""
        for member in members:
            return_value += "%s %s %s\n" % (command, list_name, member)

        return return_value

    def _process_members_operations(self, members_operations):
        zmprov = subprocess.Popen(EmailList.ZIMBRA_ZMPROV, shell=False, stdin=subprocess.PIPE, stdout=subprocess.PIPE)

        commands = ""
        for operation in members_operations:
            if operation['operation'] == 'add':
                commands += self._create_members_add_command(operation['name'], operation['entries'])
            if operation['operation'] == 'remove':
                commands += self._create_members_remove_command(operation['name'], operation['entries'])
            if operation['operation'] == 'set':
                commands += self._create_members_set_command(operation['name'], operation['entries'])

            if operation['operation'] == "":
                continue

        zmprov.communicate(input=commands)

    def process_message(self, message):
        instructions = message.body
        reply_to = message.header['uuid']

        try:
            logger.debug('process message: %s', message)

            if 'lists' in instructions:
                self._process_lists_operations(instructions['lists'])

            if 'members' in instructions:
                self._process_members_operations(instructions['members'])

            self.send_reply(reply_to, 0, "Done")

        except Exception as ex:
            self.send_reply(reply_to, -1, repr(ex))

    ZIMBRA_ZMPROV = '/opt/zimbra/bin/zmprov'


def initialize():
    return EmailList().identification()